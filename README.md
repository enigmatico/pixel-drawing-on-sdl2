# Pixel Drawing on SDL2

Pixel Drawing experiment using SDL2. It's kinda slow, but it works.

All it does is creating a SDL surface from an array of RGBA bytes and
then transform said surface into a texture, that it's then rendered.

Since it generates the texture from an array of pixels, the memory
can be altered directly.

The only issue is that this process is very slow, but it works.
