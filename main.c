#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <time.h>
#include <SDL2/SDL.h>

/* DON'T tell me to use stdbool, I love doing this! */
enum boolean{false,true};
typedef enum boolean bool;

/* Window Width */
#define WINDOW_WIDTH 1024
/* Window Height */
#define WINDOW_HEIGHT 768
/* The size of the graphic memory array */
#define VGASIZE 4*(WINDOW_WIDTH*WINDOW_HEIGHT)
/* The size, in bytes, of one row of pixels */
#define VGAROW 4*WINDOW_WIDTH

/* Some masks whose order depend on the architecture endianess */
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    #define rmask 0xff000000
    #define gmask 0x00ff0000
    #define bmask 0x0000ff00
    #define amask 0x000000ff
#else
    #define rmask 0x000000ff
    #define gmask 0x0000ff00
    #define bmask 0x00ff0000
    #define amask 0xff000000
#endif

int main(void) {
    SDL_Event event;
    SDL_Renderer *renderer;
    SDL_Window *window;
    SDL_Texture *canvas;
    SDL_Surface *canvas_surf;
    int i = 0;
    int res = 0;
    int shift = 0;
    char pixel = 0;
    char vgamem[VGASIZE];
    time_t t;

    /* Initialize the PRNG */
    srand((unsigned) time(&t));
    /* Initialize the graphic memory array */
    memset(vgamem, 0x00, VGASIZE);
    
    /* Set some pixels (The 4th byte is the alpha since the color
        will be RGBA) */
    for(int x = 0; x < VGASIZE; ++x)
    {
        if(!(x+1)%4)
            vgamem[x] = 0xFF;
            
        vgamem[x] = (x+shift)%255;
    }
    
    /* Initialize SDL*/
    res = SDL_Init(SDL_INIT_VIDEO);
    if(res)
    {
        printf("Error: %i 0x%#02x", res, res);
        return res;
    }
    
    /* Create a new window and renderer */
    window = SDL_CreateWindow("NO SIGNAL", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        WINDOW_WIDTH, WINDOW_HEIGHT, 0);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_TARGETTEXTURE);
    
    /* Create the main surface from the graphic memory array */
    canvas_surf = SDL_CreateRGBSurfaceFrom((void*)vgamem, WINDOW_WIDTH, WINDOW_HEIGHT, 32, VGAROW,
                                             rmask, gmask, bmask, amask);
    
    /* Create a texture from the main surface */
    canvas = SDL_CreateTextureFromSurface(renderer, canvas_surf);
    
    /* Initialize the scene */
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);
    
    /* Main loop (until the window is closed) */
    while (!SDL_PollEvent(&event) || event.type != SDL_QUIT)
    {
        // Generate a TV static effect
        for(int x = 0; x < VGASIZE; x+=4)
        {
            /*if(!(x+1)%4)
                vgamem[x] = 0xFF;*/
            pixel = rand() % 255;
            
            vgamem[x] = (pixel+shift)%255;      //RED
            vgamem[x+1] = (pixel+shift)%255;    //GREEN
            vgamem[x+2] = (pixel+shift)%255;    //BLUE
            vgamem[x+3] = 255;                  //ALPHA
        }
        
        // Destroy the previous texture
        SDL_DestroyTexture(canvas);
        
        // Generate a new texture with the new data
        canvas = SDL_CreateTextureFromSurface(renderer, canvas_surf);
        
        // Select our texture as the main target for our operations
        SDL_SetRenderTarget(renderer, canvas);
        
        // Set the background color
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);
        
        // Draw a diagonal red line using RenderDrawPoint
        // (Also this won't work well with non-square resolutions)
        SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
        for (i = 0; i < WINDOW_WIDTH; ++i)
            SDL_RenderDrawPoint(renderer, i, i);
        
        // Render the texture
        SDL_SetRenderTarget(renderer, NULL);
        SDL_RenderCopyEx(renderer, canvas, NULL, NULL, 0, NULL, 0);
        SDL_RenderPresent(renderer);
        
        shift+=2;
    }
    
    // Free the memory and exit
    SDL_DestroyTexture(canvas);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return EXIT_SUCCESS;
}
